<%-- 
    Document   : chart-page
    Created on : 26/06/2017, 21:36:43
    Author     : Fernando
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="en">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Cake&#39;s Dream is Beautiful Template " name="description">
    <meta content="" name="author">
    <link href="assets/images/favicon-32x32.png" rel="shortcut icon">
    <title>Carrinho</title>
    <link href="assets/stylesheets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/stylesheets/css/font-family.css" rel="stylesheet">
    <link href="assets/stylesheets/css/responsive.css" rel="stylesheet">
    <link href="assets/stylesheets/css/slick.css" rel="stylesheet">
    <link href="assets/stylesheets/css/slick-theme.css" rel="stylesheet">
    <link href="assets/stylesheets/css/style.css" rel="stylesheet">
    <link href="assets/stylesheets/css/animate.css" rel="stylesheet">
    <link href="assets/javascripts/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css">
    <link href="assets/stylesheets/css/global.css" rel="stylesheet">
    <link href="assets/stylesheets/css/effect2.css" rel="stylesheet" type="text/css">
    <link href="assets/stylesheets/css/login.css" rel="stylesheet">
    <script src="assets/javascripts/modernizr.custom.js"></script>
  </head>
  <body class="demo-1">
    <div class="ip-container" id="ip-container">
      <!--initial header-->
      <header class="ip-header">
        <div class="ip-loader">
          <svg class="ip-inner" height="60px" viewbox="0 0 80 80" width="60px"><path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"></path><path class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z" id="ip-loader-circle"></path></svg>
        </div>
      </header>
      <!--main content-->
      <div class="ip-main">
        <div class="top-highlight hide">
          &nbsp;
        </div>
        <!-- Start Header Cake -->
        <section class="header-wrapper">
          <header class="wrap-header">
            <div class="top-absolute">
              <div class="top-header">
                <div class="container">
                  <div class="navbar-header visible-xs">
                    <button class="navbar-toggle toggle-cake show-menu"><span class="sr-only">Toggle Navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand navbar-cake" href="#"><img alt="Logo-Cupcakes" src="assets/images/logo-100.png"></a>
                  </div>
                  <nav>
                    <ul class="header-nav hidden-xs">
                      <li>
                        <a href="index.jsp">Início</a>
                      </li>
                      <li>
                        <a href="shop.jsp">Loja</a>
                      </li>
                      
                      <li class="pad-top-0i">
                        <img alt="Logo-Cupcakes" src="assets/images/logo-150.png">
                      </li>
                      <li>
                        <a href="blog-center.jsp">Blog</a>
                      </li>
                      
                     
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Login<span class="caret"></span></a>
                        <ul id="login-dp" class="dropdown-menu">
                          <li>

                            <div class="row">

                              <div class="col-md-12">

                                <form class="form" role="form" method="post" action="login.jsp" accept-charset="UTF-8" id="login-nav">
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail2">Usuário</label>
                                    <input type="text" name="uname" value="" class="form-control" placeholder="Usuário" required>
                                  </div>
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputPassword2">Senha</label>
                                    <input type="password" name="pass" class="form-control" placeholder="Senha" required>
                                    <div class="help-block text-right">
                                      <a href="">
                                        <font color="red">Esqueceu a senha?</font>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <button type="submit" value="Login" class="btn btn-danger btn-block">Entrar</button>
                                  </div>
                                  <div class="checkbox">
                                    <label>
											 <input type="checkbox"><font color="red" size="1"> Mantenha-me logado</font>
											 </label>
                                  </div>
                                </form>
                              </div>
                              <div class="bottom text-center">
                                <a href="reg.jsp"><b><font color="red" size="1">Cadastre-se</font></b></a>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                      </ul>
                      </nav>
                      <!-- Start Mega Menu Cake -->
                  <div class="mega-menu hide">
                    <div class="tittle-mega">
                      <h4>
                        - Mega Menu -
                      </h4>
                    </div>
                    <div class="container">
                      <div class="row">
                        <div class="col-sm-4">
                          <ul class="list-mega">
                            <li class="bottom-red-border">
                              Blog
                            </li>
                            <li>
                              <a href="blog.jsp">Blog Left Content</a>
                            </li>
                            <li>
                              <a href="blog-right-content.html">Blog Right Content</a>
                            </li>
                            <li>
                              <a href="blog-center.html">Blog Center</a>
                            </li>
                          </ul>
                        </div>
                        <div class="col-sm-4">
                          <ul class="list-mega">
                            <li class="bottom-red-border">
                              Gallery
                            </li>
                            <li>
                              <a href="gallery.html">Gallery 3 Column</a>
                            </li>
                            <li>
                              <a href="gallery-4-column.html">Gallery 4 Column</a>
                            </li>
                            <li>
                              <a href="gallery-dot.html">Gallery With Text</a>
                            </li>
                          </ul>
                        </div>
                        <div class="col-sm-4">
                          <ul class="list-mega">
                            <li class="bottom-red-border">
                              OTHER PAGEs
                            </li>
                            <li>
                              <a href="chart-page.html">Chart Page</a>
                            </li>
                            <li>
                              <a href="product-details-page.html">Product Details</a>
                            </li>
                            <li>
                              <a href="privacy-policy.html">Privacy Policy</a>
                            </li>
                            <li>
                              <a href="terms-of-use.html">Terms Of Use</a>
                            </li>
                            <li>
                              <a href="404.html">404</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="div text-center">
                        <button class="btn btn-pink-cake mar-top-20 close-menu">Close Themes</button>
                      </div>
                    </div>
                  </div>
                  <!-- End Mega Menu Cake -->
                </div>
              </div>
              <div class="triangle">
                &nbsp;
              </div>
            </div>
            <div class="tittle-sub-top pad-top-150">
              <div class="container">
                Início /
                <h1>
                  Carrinho
                </h1>
                <h2>
                  Doce
                </h2>
              </div>
            </div>
          </header>
          <div class="purple-arrow">
            &nbsp;
          </div>
          <div class="chart-cake">
            <div class="container">
              <table class="table table-bordered table-hover hidden-xs">
                <thead>
                  <tr>
                    <th>
                      Nº
                    </th>
                    <th>
                      Produto
                    </th>
                    <th>
                      Descrição
                    </th>
                    <th>
                      Data
                    </th>
                    <th>
                      Qtd
                    </th>
                    <th>
                      Preço
                    </th>
                    <th>
                      Status
                    </th>
                  </tr>
                </thead>
                <tbody>
                  <tr>
                    <td>
                      1
                    </td>
                    <td>
                      <img alt="Cake-one" class="img-100px" src="assets/images/cake-one-buy.png">
                    </td>
                    <td class="chart-description">
                      <h4 class="mar-btm-0">
                        Torta Três Porquinhos na Lama
                      </h4>
                      <ul class="star normal-heading">
                        <li>
                          <div class="icon-star-active">
                            &nbsp;
                          </div>
                        </li>
                        <li>
                          <div class="icon-star-active">
                            &nbsp;
                          </div>
                        </li>
                        <li>
                          <div class="icon-star-active">
                            &nbsp;
                          </div>
                        </li>
                        <li>
                          <div class="icon-star-active">
                            &nbsp;
                          </div>
                        </li>
                        <li>
                          <div class="icon-star-active">
                            &nbsp;
                          </div>
                        </li>
                        <li>
                          <span class="grey-color"><i>Recomendado</i></span>
                        </li>
                      </ul>
                      <p class="mar-top-10 pad-top-10 top-dashed">
                      Torta com do bolo de chocolate especial da casa umidecido com especiarias e toques de cacau, recheado com uma camada de
                          doce de chocolate e outra com doce de coco, decorado com camada de chocolate blend, biscoitos de
                          chocolate e porquinhos de leite ninho.  
                      </p>
                    </td>
                    <td>
                      03 junho 2017
                    </td>
                    <td>
                      1
                    </td>
                    <td>
                      R$100,00
                    </td>
                    <td class="chart-center">
                      <button class="btn btn-pink-cake mar-right-10">Checkout</button>
                    </td>
                  </tr>
                  <tr>
                    
                  </tr>
                </tbody>
              </table>
              <div class="visible-xs">
                <div class="top-cake-table">
                  <div class="top-cake-no">
                    No : 1
                  </div>
                  <div class="top-cake-product">
                    Product : Purple Cake
                    <ul class="star normal-heading">
                      <li>
                        <div class="icon-star-active">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-active">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-active">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-disable">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-disable">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <span class="grey-color"><i>Required</i></span>
                      </li>
                    </ul>
                  </div>
                  <div class="top-cake-desription">
                    Description : 
                    <p>
                      Toffee sugar plum halvah liquorice brownie gummies chocolate bar muffin candy canes. Dessert jelly-o tootsie roll jelly sesame snaps icing.
                    </p>
                  </div>
                  <div class="top-cake-img">
                    <img alt="Cake-one" class="img-150px" src="assets/images/cake-one-buy.png">
                  </div>
                  <div class="top-cake-button text-center">
                    <button class="btn btn-pink-cake mar-right-10">Checkout</button>
                  </div>
                </div>
                <div class="top-cake-table">
                  <div class="top-cake-no">
                    No : 2
                  </div>
                  <div class="top-cake-product">
                    Product : Pink Cake
                    <ul class="star normal-heading">
                      <li>
                        <div class="icon-star-active">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-active">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-active">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-disable">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-disable">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <span class="grey-color"><i>Required</i></span>
                      </li>
                    </ul>
                  </div>
                  <div class="top-cake-desription">
                    Description : 
                    <p>
                      Toffee sugar plum halvah liquorice brownie gummies chocolate bar muffin candy canes. Dessert jelly-o tootsie roll jelly sesame snaps icing.
                    </p>
                  </div>
                  <div class="top-cake-img">
                    <img alt="Cake-one" class="img-150px" src="assets/images/cake-two-buy.png">
                  </div>
                  <div class="top-cake-button text-center">
                    <button class="btn btn-pink-cake mar-right-10">Checkout</button>
                  </div>
                </div>
                <div class="top-cake-table">
                  <div class="top-cake-no">
                    No : 3
                  </div>
                  <div class="top-cake-product">
                    Product : Pink Cake
                    <ul class="star normal-heading">
                      <li>
                        <div class="icon-star-active">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-active">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-active">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-disable">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <div class="icon-star-disable">
                          &nbsp;
                        </div>
                      </li>
                      <li>
                        <span class="grey-color"><i>Required</i></span>
                      </li>
                    </ul>
                  </div>
                  <div class="top-cake-desription">
                    Description : 
                    <p>
                      Toffee sugar plum halvah liquorice brownie gummies chocolate bar muffin candy canes. Dessert jelly-o tootsie roll jelly sesame snaps icing.
                    </p>
                  </div>
                  <div class="top-cake-img">
                    <img alt="Cake-one" class="img-150px" src="assets/images/cake-three-buy.png">
                  </div>
                  <div class="top-cake-button text-center">
                    <button class="btn btn-pink-cake mar-right-10">Pagar</button>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End Header Cake -->
        <div class="green-arrow">
          &nbsp;
        </div>
        <!-- Start More Cake -->
        <section class="more-cake text-center">
          <div class="container">
            <img alt="Cake-White" class="mar-top-20" src="assets/images/cake-white.png">
            <p class="mar-top-20 mar-btm-20">
              Você pode&nbsp;<b>olhar também</b>&nbsp;estas delícias.
            </p>
            <div class="row">
              <div class="col-sm-4">
                <div class="more-product">
                  <img alt="More-Product" class="img-100" src="assets/images/bread.png">
                </div>
                <div class="detail-product">
                  <div class="row">
                    <div class="col-sm-6">
                      <h1 class="normal-heading green-color">
                        $1
                      </h1>
                    </div>
                    <div class="col-sm-6">
                      <b>Pães </b><i>Caseiros</i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="more-product">
                  <img alt="More-Product" class="img-100" src="assets/images/cookies.png">
                </div>
                <div class="detail-product">
                  <div class="row">
                    <div class="col-sm-6">
                      <h1 class="normal-heading green-color">
                        $3
                      </h1>
                    </div>
                    <div class="col-sm-6">
                      <b>Cookies </b><i>Crocantes</i>
                    </div>
                  </div>
                </div>
              </div>
              <div class="col-sm-4">
                <div class="more-product">
                  <img alt="More-Product" class="img-100" src="assets/images/cupcake.png">
                </div>
                <div class="detail-product">
                  <div class="row">
                    <div class="col-sm-6">
                      <h1 class="normal-heading green-color">
                        $5
                      </h1>
                    </div>
                    <div class="col-sm-6">
                      <b>Deliciosos </b><i>Cupcakes</i>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End More Cake -->
        <div class="green-arrow">
          &nbsp;
        </div>
        <div class="pad-top-150"></div>
        <!-- End Option Cake --><!-- Start Footer Cake -->
        <footer>
          <div class="triangle-no-animate">
            &nbsp;
          </div>
          <div class="container">
            <div class="abs-logo-footer">
              <img alt="Logo-Cake" src="assets/images/cake-white-lg.png">
            </div>
            <div class="top-footer">
              <div class="row">
                <div class="col-sm-6">
                  <img alt="Logo-White" class="img-cake-center-res mar-btm-20" src="assets/images/logo-white.png">
                </div>
                <div class="col-sm-6 text-right">
                  <ul class="sosmed-cake">
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-facebook">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-twitter">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-behance">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-dribbble">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-pinterest">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="line-top-white mar-btm-20 mar-top-20">
              &nbsp;
            </div>
            <div class="content-about-footer">
              <!-- Column -->
              <div class="col-sm-4">
                <h4>
                  Engenho Doce
                </h4>
                <p class="mar-btm-20">
                   Somos uma equipe que adora fazer doces artesanais e se juntou para atender quem aprecia doces de alta qualidade e sabor inigualável.
                </p>
                <p class="mar-btm-20">
                  Centro <br>Goiânia-Goiás, Brasil<br>
                </p>
                <p class="mar-btm-20">
                  Tel : <strong>062 32035723</strong>
                </p>
              </div>
              
              <!-- Column -->
              <div class="col-sm-4">
                <ul class="list-link-home">
                  <li>
                    <a href="shop.html">Loja</a>
                  </li>
                  
                  <li>
                    <a href="privacy-policy.html">Privacidade</a>
                  </li>
                  <li>
                    <a href="terms-of-use.html">Termos de Uso</a>
                  </li>
                  <li>
                    <a href="blog-center.html">Blog</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="logo-dn">
              <img alt="Delip Nugraha" src="assets/images/dn.png">
            </div>
          </div>
        </footer>
        <!-- End Option Cake -->
      </div>
    </div>
    <script src="assets/javascripts/jquery.js"></script>
    <script src="assets/javascripts/fancybox/jquery.fancybox.pack.js"></script>
    <script src="assets/javascripts/slick.js"></script>
    <script src="assets/javascripts/wow/wow.js"></script>
    <script src="assets/javascripts/custom.js"></script>
    <script src="assets/javascripts/bootstrap.js"></script>
    <script src="assets/javascripts/classie.js"></script>
    <script src="assets/javascripts/pathLoader.js"></script>
    <script src="assets/javascripts/main.js"></script>
    <script type="text/javascript">
      new WOW().init();
    </script>
  </body>
</html>
