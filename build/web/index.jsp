<%-- 
    Document   : index
    Created on : 26/06/2017, 20:41:44
    Author     : Fernando
--%>

<%@page contentType="text/html" pageEncoding="UTF-8"%>
<!DOCTYPE html>
<html class="no-js" lang="pt">
  <head>
    <meta charset="utf-8">
    <meta content="width=device-width, initial-scale=1.0" name="viewport">
    <meta content="Engenho Doce" name="description">
    <meta content="" name="author">
    <link href="assets/images/favicon-32x32.png" rel="shortcut icon">
    <title>Engenho Doce</title>
    <link href="assets/stylesheets/css/bootstrap.css" rel="stylesheet">
    <link href="assets/stylesheets/css/font-family.css" rel="stylesheet">
    <link href="assets/stylesheets/css/responsive.css" rel="stylesheet">
    <link href="assets/stylesheets/css/slick.css" rel="stylesheet">
    <link href="assets/stylesheets/css/slick-theme.css" rel="stylesheet">
    <link href="assets/stylesheets/css/style.css" rel="stylesheet">
    <link href="assets/stylesheets/css/animate.css" rel="stylesheet">
    <link href="assets/javascripts/fancybox/jquery.fancybox.css" rel="stylesheet" type="text/css">
    <link href="assets/stylesheets/css/global.css" rel="stylesheet">
    <link href="assets/stylesheets/css/effect2.css" rel="stylesheet" type="text/css">
    <link href="assets/stylesheets/css/login.css" rel="stylesheet">
    <script src="assets/javascripts/modernizr.custom.js"></script>
  </head>
  <body class="demo-1">
    <div class="ip-container" id="ip-container">
      <!--initial header-->
      <header class="ip-header">
        <div class="ip-loader">
          <svg class="ip-inner" height="60px" viewbox="0 0 80 80" width="60px"><path class="ip-loader-circlebg" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z"></path><path class="ip-loader-circle" d="M40,10C57.351,10,71,23.649,71,40.5S57.351,71,40.5,71 S10,57.351,10,40.5S23.649,10,40.5,10z" id="ip-loader-circle"></path></svg>
        </div>
      </header>
      <!--main content-->
      <div class="ip-main">
        <div class="top-highlight hide">
          &nbsp;
        </div>
        <!-- Start Header Cake -->
        <section class="header-wrapper">
          <header class="wrap-header">
            <div class="top-absolute">
              <div class="top-header">
                <div class="container">
                  <div class="navbar-header visible-xs">
                    <button class="navbar-toggle toggle-cake show-menu"><span class="sr-only">Toggle Navigation</span><span class="icon-bar"></span><span class="icon-bar"></span><span class="icon-bar"></span></button><a class="navbar-brand navbar-cake" href="#"><img alt="Logo-Cupcakes" src="assets/images/logo-100.png"></a>
                  </div>
                  <nav>
                    <ul class="header-nav hidden-xs">
                      <li>
                        <a href="index.jsp">Início</a>
                      </li>
                      <li>
                        <a href="shop.jsp">Loja</a>
                      </li>
                      
                      <li class="pad-top-0i">
                        <img alt="Logo-Cupcakes" src="assets/images/logo-150.png">
                      </li>
                      <li>
                        <a href="blog-center.jsp">Blog</a>
                      </li>
                      
                     
                      <li class="dropdown">
                        <a href="#" class="dropdown-toggle" data-toggle="dropdown">Login<span class="caret"></span></a>
                        <ul id="login-dp" class="dropdown-menu">
                          <li>

                            <div class="row">

                              <div class="col-md-12">

                                <form class="form" role="form" method="post" action="login.jsp" accept-charset="UTF-8" id="login-nav">
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputEmail2">Usuário</label>
                                    <input type="text" name="uname" value="" class="form-control" placeholder="Usuário" required>
                                  </div>
                                  <div class="form-group">
                                    <label class="sr-only" for="exampleInputPassword2">Senha</label>
                                    <input type="password" name="pass" class="form-control" placeholder="Senha" required>
                                    <div class="help-block text-right">
                                      <a href="">
                                        <font color="red">Esqueceu a senha?</font>
                                      </a>
                                    </div>
                                  </div>
                                  <div class="form-group">
                                    <button type="submit" value="Login" class="btn btn-danger btn-block">Entrar</button>
                                  </div>
                                  <div class="checkbox">
                                    <label>
											 <input type="checkbox"><font color="red" size="1"> Mantenha-me logado</font>
											 </label>
                                  </div>
                                </form>
                              </div>
                              <div class="bottom text-center">
                                <a href="reg.jsp"><b><font color="red" size="1">Cadastre-se</font></b></a>
                              </div>
                            </div>
                          </li>
                        </ul>
                      </li>
                      </ul>
                      </nav>
                      <!-- Start Mega Menu Cake -->
                  <div class="mega-menu hide">
                    <div class="tittle-mega">
                      <h4>
                        - Mega Menu -
                      </h4>
                    </div>
                    <div class="container">
                      <div class="row">
                        <div class="col-sm-4">
                          <ul class="list-mega">
                            <li class="bottom-red-border">
                              Blog
                            </li>
                            <li>
                              <a href="blog.jsp">Blog Left Content</a>
                            </li>
                            <li>
                              <a href="blog-right-content.html">Blog Right Content</a>
                            </li>
                            <li>
                              <a href="blog-center.html">Blog Center</a>
                            </li>
                          </ul>
                        </div>
                        <div class="col-sm-4">
                          <ul class="list-mega">
                            <li class="bottom-red-border">
                              Gallery
                            </li>
                            <li>
                              <a href="gallery.html">Gallery 3 Column</a>
                            </li>
                            <li>
                              <a href="gallery-4-column.html">Gallery 4 Column</a>
                            </li>
                            <li>
                              <a href="gallery-dot.html">Gallery With Text</a>
                            </li>
                          </ul>
                        </div>
                        <div class="col-sm-4">
                          <ul class="list-mega">
                            <li class="bottom-red-border">
                              OTHER PAGEs
                            </li>
                            <li>
                              <a href="chart-page.html">Chart Page</a>
                            </li>
                            <li>
                              <a href="product-details-page.html">Product Details</a>
                            </li>
                            <li>
                              <a href="privacy-policy.html">Privacy Policy</a>
                            </li>
                            <li>
                              <a href="terms-of-use.html">Terms Of Use</a>
                            </li>
                            <li>
                              <a href="404.html">404</a>
                            </li>
                          </ul>
                        </div>
                      </div>
                      <div class="div text-center">
                        <button class="btn btn-pink-cake mar-top-20 close-menu">Close Themes</button>
                      </div>
                    </div>
                  </div>
                  <!-- End Mega Menu Cake -->
                </div>
              </div>
              <div class="triangle">
                &nbsp;
              </div>
            </div>
            <div class="tittle-cake text-center pad-top-150">
              <div class="container">
                <h2>
                  Engenho
                </h2>
                <h1>
                  Doce
                </h1>
              </div>
            </div>
            <div class="slider-cake">
              <div class="container pad-md-100">
                <div class="center">
                  <div class="img-relative">
                    <a href="chart-page.jsp" ><img alt="Cake-one" src="assets/images/cake-one.png"></a>
                    <div class="price-cake hidden-xs">
                      <p>
                        #1
                      </p>
                    </div>
                  </div>
                  <div>
                   <a href="chart-page.jsp"> <img alt="Cake-Two" src="assets/images/cake-two.png"></a>
                  </div>
                  <div>
                    <a href="chart-page.jsp"><img alt="Cake-Three" src="assets/images/cake-three.png"></a>
                  </div>
                  <div>
                    <a href="chart-page.jsp"><img alt="Cake-Four" src="assets/images/cake-four.png"></a>
                  </div>
                  <div>
                    <a href="chart-page.jsp"><img alt="Cake-Five" src="assets/images/cake-five.png"></a>
                  </div>
                </div>
              </div>
            </div>
            <div class="green-table mar-to-top">
              &nbsp;
            </div>
            <div class="green-arrow">
              &nbsp;
            </div>
          </header>
        </section>
        <!-- End Header Cake --><!-- Start About Cake -->
        <section class="about-cake">
          <div class="container">
            <!-- About Content -->
            <h2 class="hide">
              &nbsp;
            </h2>
            <div class="about-content">
              <img alt="Cake-White" src="assets/images/cake-white.png">
              <p>
                 A Engenho Doce veio para adoçar sua vida com nossos doces artesanais.
              </p>
            </div>
          </div>
        </section>
        <!-- End About Cake --><!-- Start Product Cake -->
        <section class="product-cake">
          <div class="container">
            <!-- Product Tittle -->
            <div class="product-tittle">
              <img alt="Cake-Purple" src="assets/images/cake-purple.png">
              <h2>
                Produtos
              </h2>
            </div>
            <!-- Product Content -->
            <div class="product-content">
              <div class="row">
                <!-- Column -->
                <div class="col-sm-4">
                  <div class="wrap-product">
                    <div class="top-product blue-cake">
                      
                      <p class="mar-top-10 mar-btm-0">
                         TORTA<br>PORQUINHOS<br>NA LAMA
                      </p>
                      <span>Tarte Petits Cochons<br>Dans L'étang</span>
                    </div>
                    <div class="bottom-product bottom-blue">
                      <div class="bottom-product-abs blue-dot">
                        <div class="button-cake">
                          <div class="blue-button-cake">
                            <button class="button-d-cake blue-button-cake"><a href="chart-page.html">Encomendar</a></button>
                          </div>
                        </div>
                      </div>
                      <div class="wrap-bottom-cake">
                        <p>
                           Torta com do bolo de chocolate especial da casa umidecido com especiarias e toques de cacau, recheado com uma camada de
                          doce de chocolate e outra com doce de coco, decorado com camada de chocolate blend, biscoitos de
                          chocolate e porquinhos de leite ninho.
                        </p>
                        <div class="blue-line"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Column -->
                <div class="col-sm-4">
                  <div class="wrap-product">
                    <div class="top-product red-cake">
                      
                      <p class="mar-top-10 mar-btm-0">
                         TORTA<br> FLOR<br>DE MORANGO
                      </p>
                      <span><i>Pie Fleur fraise</i></span>
                    </div>
                    <div class="bottom-product bottom-red">
                      <div class="bottom-product-abs pink-dot">
                        <div class="button-cake">
                          <div class="blue-button-cake">
                            <button class="button-d-cake pink-button-cake"><a href="chart-page.html">Encomendar</a></button>
                          </div>
                        </div>
                      </div>
                      <div class="wrap-bottom-cake">
                        <p>
                           Torta feita com do Pão de ló especial da casa umedecido com especiarias e toques de baunilha, recheado com uma camada de
                          doce de morango e outra de chantilly com morangos frescos, decorado com lascas de morango que,
                          unidas, formam uma flor.
                        </p>
                        <div class="red-line"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Column -->
                <div class="col-sm-4">
                  <div class="wrap-product">
                    <div class="top-product orange-cake">
                       
                      <p class="mar-top-10 mar-btm-0">
                         MINI TORTA<br>OLHO DE<br>SOGRA
                      </p>
                      <span><i>Mini-tarte Les Yeux<br> de la Belle-mère<i></span>
                    </div>
                    <div class="bottom-product bottom-orange">
                      <div class="bottom-product-abs orange-dot">
                        <div class="button-cake">
                          <div class="blue-button-cake">
                            <button class="button-d-cake orange-button-cake"><a href="chart-page.html">Encomendar</a></button>
                          </div>
                        </div>
                      </div>
                      <div class="wrap-bottom-cake">
                        <p>
                           Torta feita com do Pão de ló especial da casa umidecido com especiarias, caramelo e toques de ameixa, recheado com uma camada
                          de doce de ameixa e outra com doce de coco, decorado com vasta camada de chantilly e ameixas passas.
                        </p>
                        <div class="orange-line"></div>
                      </div>
                    </div>
                  </div>
                </div>
                <!-- Column Tittle -->
                <div class="col-sm-12">
                  <p class="text-content text-center">
                     Nossos produtos são selecionados e apresentam as melhores características de fabricação, garantindo sua beleza, sabor único
                    e a satisfação do cliente.
                  </p>
                </div>
              </div>
            </div>
          </div>
        </section>
        <!-- End Product Cake --><!-- Start News Cake -->
        <section class="news-cake">
          <div class="triangle-no-animate">
            &nbsp;
          </div>
          <!-- News Content -->
          <div class="new-cake-content mar-top-20">
            <!-- Tittle News Content -->
            <div class="tittle-cake text-center">
              <div class="container">
                <img alt="Cake-White" src="assets/images/cake-white.png">
                <h2>
                  Mural
                </h2>
              </div>
            </div>
            <!-- Content News-->
            <div class="container mar-top-20">
              <div class="row">
                <div class="col-sm-6 no-pad-right">
                  <div class="left-news">
                    
                     
                    
                  </div>
                  <div class="right-news">
                    <div class="text-table">
                      <p> <!-- Linka ao Shop-->
                        <a href="shop.html"><span class="discount">15%<span class="percent"></span><br></span><span class="sale">Pague a vista e receba 15% de desconto em bolos de aniversário e casamentos!</span></a>
                      </p>
                    </div>
                    <div class="text-table dot-background">
                      <p>
                        <img alt="Client" src="assets/images/client.png">
                      </p>
                    </div>
                  </div>
                </div>
                <div class="col-sm-6 no-pad-left">
                  <div class="top-news-right">
                    <div class="left-news-right">
                      <div class="text-table">
                        <a class="fancybox" data-fancybox-group="contentnews" href="assets/images/ice-cream.png">
                          <div class="wizz-effect wizz-orange">
                            <div class="wrap-info">
                              Casamentos
                            </div>
                          </div>
                        </a>
                        <p>
                          <img alt="Ice Cream" class="img-100" src="assets/images/wedding.png">
                        </p>
                      </div>
                    </div>
                    <div class="right-news-right">
                      <div class="text-table">
                        <a class="fancybox" data-fancybox-group="contentnews" href="assets/images/ice-cream-cake.png">
                          <div class="wizz-effect wizz-green">
                            <div class="wrap-info">
                              Aniversário
                            </div>
                          </div>
                        </a>
                        <p>
                          <img alt="Ice Cream Cake" class="img-100" src="assets/images/birthday.png">
                        </p>
                      </div>
                    </div>
                  </div>
                  <div class="bottom-new-right">
                    <div class="quote">
                      <div>
                        <span class="mar-right-10"><img alt="Quote" class="Quote" src="assets/images/quote.png"></span>
                        <p>
                          <span class="bold-font-lg">Gisana,</span><span> Empresária</span>
                        </p>
                        <p>
                          É a melhor torta que já comi na VIDA! <br>Cada mordida, um pedacinho do Ceú.
                        </p>
                      </div>
                      <div>
                        <span class="mar-right-10"><img alt="Quote" class="Quote" src="assets/images/quote.png"></span>
                        <p>
                          <span class="bold-font-lg">Walison, </span><span> Professor </span>
                        </p>
                        <p>
                          Se você quer impressionar alguém, Pie Fleur Fraise é a pedida certa !
                        </p>
                      </div>
                      <div>
                        <span class="mar-right-10"><img alt="Quote" class="Quote" src="assets/images/quote.png"></span>
                        <p>
                          <span class="bold-font-lg">Yan, </span><span> Universitário </span>
                        </p>
                        <p>
                          Incrivelmente delicioso!<br> A vontade é de comer todos os dias!
                        </p>
                      </div>
                    </div>
                  </div>
                </div>
              </div>
            </div>
            <!-- End Content News-->
          </div>
          <!-- End News Content-->
        </section>
        <!-- End News Cake --><!-- Start Option Cake -->
        <section class="option">
          <!-- Tittle Option -->
          <div class="green-table pad-top-10 pad-btm-10">
            <div class="container">
              <div class="tittle-cake text-center">
                <img alt="Cake-White" src="assets/images/cake-white.png">
                <h2>
                  Especialidades
                </h2>
              </div>
            </div>
          </div>
          <div class="green-arrow"></div>
          <!-- Option Content -->
          <div class="option-content">
            <div class="container">
              <!-- Column -->
              <div class="col-sm-4">
                <div class="messes">
                   
                  <div class="messes-show"></div>
                  
                  <img alt="Birthday-Cake" src="assets/images/birthday-cake.png">
                </div>
                <h4 class="green-color">
                 <a href="shop.html"> Bolos</a>
                </h4>
                <div class="line-temp line-green-sm">
                  &nbsp;
                </div>
                <p class="text-center mar-top-10">
                  Bolos ao seu pedido.
                </p>
              </div>
              <!-- Column -->
              <div class="col-sm-4">
                <div class="messes">
                  <div class="messes-show"></div>
                   <img alt="Pie" src="assets/images/pie.png">
                </div>
                <h4 class="orange-color">
                  <a href="shop.html">Torta</a>
                </h4>
                <div class="line-temp line-orange-sm">
                  &nbsp;
                </div>
                <p class="text-center mar-top-10">
                  Torta para sua festa.
                </p>
              </div>
              <!-- Column -->
              <div class="col-sm-4">
                <div class="messes">
                  <div class="messes-show"></div>
                   <img alt="Candy" src="assets/images/candy-canes.png">
                </div>
                <h4 class="blue-color">
                  <a href="shop.html">Doces</a>
                </h4>
                <div class="line-temp line-blue-sm">
                  &nbsp;
                </div>
                <p class="text-center mar-top-10">
                  Melhores Doces para incrementar aquela reunião com os amigos.
                </p>
              </div>
              <!-- Column -->
              <div class="col-sm-4">
                <div class="messes">
                  <div class="messes-show"></div>
                   <img alt="Cookie" src="assets/images/cookies.png">
                </div>
                <h4 class="pink-color">
                 <a href="shop.html">Cookies</a>
                </h4>
                <div class="line-temp line-pink-sm">
                  &nbsp;
                </div>
                <p class="text-center mar-top-10">
                  Fazemos Cookies da sua escolha.
                </p>
              </div>
              <!-- Column -->
              <div class="col-sm-4">
                <div class="messes">
                  <div class="messes-show"></div>
                   <img alt="Bread" src="assets/images/bread.png">
                </div>
                <h4 class="purple-color">
                  <a href="shop.html"> Pães</a>
                </h4>
                <div class="line-temp line-purple-sm">
                  &nbsp;
                </div>
                <p class="text-center mar-top-10">
                  Pães com recheio é uma de nossas especidades.
                </p>
              </div>
              <!-- Column -->
              <div class="col-sm-4">
                <div class="messes">
                  <div class="messes-show"></div>
                   <img alt="Cupcake" src="assets/images/cupcake.png">
                </div>
                <h4 class="dpurple-color">
                  <a href="chart-page.html">Cupcakes</a>
                </h4>
                <div class="line-temp line-dpurple-sm">
                  &nbsp;
                </div>
                <p class="text-center mar-top-10">
                   Que tal um Cupcake de Chocolate?
                </p>
              </div>
            </div>
          </div>
        </section>
        <!-- End Option Cake --><!-- Start Team Cake -->
        <hr class="style15">
        <section class="abouts-cake">
          <div class="tittle-cake text-center">
            <div class="container">
              <img alt="Cake-Pink" src="assets/images/cake-pink.png">
              <h2 class="pink-color">
                Nosso Time !
              </h2>
            </div>
          </div>
          <div class="container mar-top-20">
            <!-- Column -->
            <div class="col-sm-4">
              <div class="img-round-about">
                <img alt="About Team" class="img-100" src="assets/images/leticia.png">
              </div>
              <h4>
               <a href="blog-center.html">Doce Lelê</a>
              </h4>
              <div class="line-pink-about">
                &nbsp;
              </div>
              <p class="text-center">
                Criadora de tortas e bolos, <br>além do talento para doces.
              </p>
            </div>
            <!-- Column -->
            <div class="col-sm-4">
              <div class="img-round-about">
                <img alt="About Team" class="img-100" src="assets/images/nadya.png">
              </div>
              <h4>
                <a href="blog-center.html">Doce Naná</a>
              </h4>
              <div class="line-pink-about">
                &nbsp;
              </div>
              <p class="text-center">
                Nossa especialista em coberturas.
              </p>
            </div>
            <!-- Column -->
            <div class="col-sm-4">
              <div class="img-round-about">
                <img alt="About Team" class="img-100" src="assets/images/nem.png">
              </div>
              <h4>
                <a href="blog-center.html">Doce Nem</a>
              </h4>
              <div class="line-pink-about">
                &nbsp;
              </div>
              <p class="text-center">
                A criadora e administradora.
              </p>
            </div>
          </div>
        </section>
        <!-- End Option Cake --><!-- Start Footer Cake -->
        <footer>
          <div class="triangle-no-animate">
            &nbsp;
          </div>
          <div class="container">
            <div class="abs-logo-footer">
              <img alt="Logo-Cake" src="assets/images/cake-white-lg.png">
            </div>
            <div class="top-footer">
              <div class="row">
                <div class="col-sm-6">
                  <img alt="Logo-White" class="img-cake-center-res mar-btm-20" src="assets/images/logo-white.png">
                </div>
                <div class="col-sm-6 text-right">
                  <ul class="sosmed-cake">
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-facebook">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-twitter">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-behance">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-dribbble">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                    <li>
                      <div class="center-sosmed">
                        <p class="icon icon-pinterest">
                          &nbsp;
                        </p>
                      </div>
                    </li>
                  </ul>
                </div>
              </div>
            </div>
            <div class="line-top-white mar-btm-20 mar-top-20">
              &nbsp;
            </div>
            <div class="content-about-footer">
              <!-- Column -->
              <div class="col-sm-4">
                <h4>
                  Engenho Doce
                </h4>
                <p class="mar-btm-20">
                   Somos uma equipe que adora fazer doces artesanais e se juntou para atender quem aprecia doces de alta qualidade e sabor inigualável.
                </p>
                <p class="mar-btm-20">
                  Centro <br>Goiânia-Goiás, Brasil<br>
                </p>
                <p class="mar-btm-20">
                  Tel : <strong>062 32035723</strong>
                </p>
              </div>
              
              <!-- Column -->
              <div class="col-sm-4">
                <ul class="list-link-home">
                  <li>
                    <a href="shop.html">Loja</a>
                  </li>
                  
                  <li>
                    <a href="privacy-policy.html">Privacidade</a>
                  </li>
                  <li>
                    <a href="terms-of-use.html">Termos de Uso</a>
                  </li>
                  <li>
                    <a href="blog-center.html">Blog</a>
                  </li>
                </ul>
              </div>
            </div>
            <div class="logo-dn">
              <img alt="Delip Nugraha" src="assets/images/dn.png">
            </div>
          </div>
        </footer>
        <!-- End Option Cake -->
      </div>
    </div>
    <script src="assets/javascripts/jquery.js"></script>
    <script src="assets/javascripts/fancybox/jquery.fancybox.pack.js"></script>
    <script src="assets/javascripts/slick.js"></script>
    <script src="assets/javascripts/wow/wow.js"></script>
    <script src="assets/javascripts/custom.js"></script>
    <script src="assets/javascripts/bootstrap.js"></script>
    <script src="assets/javascripts/classie.js"></script>
    <script src="assets/javascripts/pathLoader.js"></script>
    <script src="assets/javascripts/main.js"></script>
    <script type="text/javascript">
      new WOW().init();
    </script>
  </body>
</html>
