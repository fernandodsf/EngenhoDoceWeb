<%-- 
    Document   : logout
    Created on : 26/06/2017, 21:02:18
    Author     : Fernando
--%>

<%
session.setAttribute("userid", null);
session.invalidate();
response.sendRedirect("index.jsp");
%>
