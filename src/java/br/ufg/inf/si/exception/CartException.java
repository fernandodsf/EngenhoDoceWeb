/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.ufg.inf.si.exception;

/**
 *
 * @author maaahad
 */
public class CartException extends Exception{
    public CartException(String msg){
        super(msg);
    }
}
