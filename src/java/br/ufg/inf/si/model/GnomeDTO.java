/*
 * Data Transfer Object for gnome
 */
package br.ufg.inf.si.model;

/**
 *
 * @author maaahad
 */
public interface GnomeDTO {
    public String getId();
    public String getGnomeType();
    public float getGnomePrice();
    public int getGnomeTotal();
    public int getGnomeAvailable();
}
