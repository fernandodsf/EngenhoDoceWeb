/*
 * Data Transfer Object for order
 */
package br.ufg.inf.si.model;

/**
 *
 * @author maaahad
 */
public interface OrderDTO {
    public String getOrderID();
    public int getAmountToBuy();
    public UserEntity getUser();
    
}
