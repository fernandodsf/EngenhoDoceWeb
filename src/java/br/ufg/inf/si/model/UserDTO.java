/*
 * Data Transfer Object for user
 */
package br.ufg.inf.si.model;

/**
 *
 * @author maaahad
 * User DataTransfer Interface
 */
public interface UserDTO {
    public String getUserName();
    public String getPassword();
    public String getPermitStatus();
    public String getOnlineStatus();
    public String getUserRole();
}
